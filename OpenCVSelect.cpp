#include <opencv2/opencv.hpp>
#include <opencv\highgui.h>

IplImage *img0,*selectedImg,*temp;
void releaseImg(IplImage *a,int x,int y);
void showImage();
IplImage* findImg(int x,int y);

int globalCoordinateX[]={50,400,400,50};	//point coordinates
int globalCoordinateY[]={50,50,400,400};
int point=-1;			//currently selected point
int nop=4;



void mouseHandler(int event, int x, int y, int flags, void *param)
{

	switch(event) {
	case CV_EVENT_LBUTTONDOWN:		//left button press
		selectedImg=findImg( x, y);
		break;

	case CV_EVENT_LBUTTONUP:	//left mouse button release
		if((selectedImg!=NULL)&&point!=-1){
			releaseImg(selectedImg,x,y);
			selectedImg=NULL;
		}
		break;


	case CV_EVENT_MOUSEMOVE:
		/* draw a rectangle*/
		if(point!=-1){
			if(selectedImg!=NULL){
				temp = cvCloneImage(selectedImg);
				cvRectangle(temp, 
					cvPoint(x - 1, y - 1), 
					cvPoint(x + 1, y + 1), 
					cvScalar(0, 0,  255,0), 2, 8, 0);

				//adjust the lines
				for(int i=0;i<nop;i++){
					if(i!=point){
						cvLine(temp,
							cvPoint(x,y ), 
							cvPoint(globalCoordinateX[i] , globalCoordinateY[i] ), 
							cvScalar(0, 0,  255,0), 1,8,0);
					}
				}
				cvShowImage("Drag & Drop", temp); 
			}
			break;
		}
		cvReleaseImage(&temp);
	}
}


IplImage* findImg(int x,int y){
	IplImage *img=img0;

	for(int i=0;i<nop;i++){
		if((x>=(globalCoordinateX[i]-2)) && (x<=(globalCoordinateX[i]+2 ))&& (y<=(globalCoordinateY[i]+2 ))&& (y<=(globalCoordinateY[i]+2 ))){
			point=i;
			break;
		}

	}
	//draw points
	for(int j=0;j<nop;j++){
		if(j!=point){
			img = cvCloneImage(img);
			cvRectangle(img, 
				cvPoint(globalCoordinateX[j] - 1, globalCoordinateY[j] - 1), 
				cvPoint(globalCoordinateX[j] + 1, globalCoordinateY[j] + 1), 
				cvScalar(255, 0,  0,0), 2, 8, 0);
		}
	}

	//draw lines
	for(int i=0;i<nop;i++){
		if(i!=point){
			for(int k=i+1;k<nop;k++){
				if(k!=point){
					img = cvCloneImage(img);
					cvLine(img,
						cvPoint(globalCoordinateX[i] , globalCoordinateY[i] ), 
						cvPoint(globalCoordinateX[k] , globalCoordinateY[k] ), 
						cvScalar(255, 0,  0,0), 1,8,0);

				}
			}
		}
	}
	return img;
}

//set new cordiinates and redeaw the scene
void releaseImg(IplImage *a,int x,int y){
	globalCoordinateX[point]=x;
	globalCoordinateY[point]=y;
	showImage();
}

//draw the scene components
void showImage(){
	IplImage *img2=img0;

	//draw the points
	for(int j=0;j<nop;j++){		
		img2 = cvCloneImage(img2);
		cvRectangle(img2, 
			cvPoint(globalCoordinateX[j] - 1, globalCoordinateY[j] - 1), 
			cvPoint(globalCoordinateX[j] + 1, globalCoordinateY[j] + 1), 
			cvScalar(255, 0,  0,0), 2, 8, 0);


		//draw the lines
		for(int k=j+1;k<nop;k++){
			img2 = cvCloneImage(img2);
			cvLine(img2,
				cvPoint(globalCoordinateX[j] , globalCoordinateY[j] ), 
				cvPoint(globalCoordinateX[k] , globalCoordinateY[k] ), 
				cvScalar(255, 0,  0,0), 1,8,0);
		}
	}
	cvShowImage("Drag & Drop", img2);
	cvReleaseImage(&img2);
}


int mainssss(int argc, char** argv)
{
	img0 = cvLoadImage("black.jpg", CV_LOAD_IMAGE_COLOR);
	cvNamedWindow("Drag & Drop", CV_WINDOW_AUTOSIZE);
	cvSetMouseCallback( "Drag & Drop", mouseHandler, NULL );
	cvShowImage("Drag & Drop", img0);
	showImage();
	cvWaitKey(0);

	cvDestroyWindow("Drag & Drop");
	cvReleaseImage(&img0);	

	return 0;
}
